﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ExercicioFinal;

namespace UnitTestProject
{
    [TestClass]
    public class OrderTest
    {
    
        [TestMethod]
        public void ItShouldCreateNewOrder()
        {
            OrderBo order = new OrderBo();

            Assert.IsInstanceOfType(order, typeof (OrderBo));
        }

        [TestMethod]
        public void ItShouldCreateNewOrderAndHaveIdentifier()
        {
            OrderBo order = new OrderBo();
            Assert.IsNotNull(order.Identifier);
        }

        [TestMethod]
        public void IdentifierNewOrderMustBeValid()
        {
            OrderBo order = new OrderBo();
            Assert.AreNotEqual(new Guid("00000000-0000-0000-0000-000000000000"), order.Identifier);
        }

        [TestMethod]
        public void MustAddItemToOrder()
        {

            try{
                OrderBo order = new OrderBo();
                SalesItemBo item = new SalesItemBo(new ProductBo("Iphone"), 1, 10);
                order.AddItem(item);

                Assert.IsTrue(true);
            }
            catch
            {
                Assert.IsTrue(false);
            }
            

        }


        [TestMethod]
        public void MustAddItemToOrderAndQuantityShouldBeEqual1()
        {

            OrderBo order = new OrderBo();
            SalesItemBo item = new SalesItemBo(new ProductBo("Iphone"), 1, 10);
            order.AddItem(item);

            Assert.AreEqual(1, order.ItensQuantity);
           

        }

        [TestMethod]
        public void MustAddItemToOrderAndQuantityShouldBeEqual2()
        {

            OrderBo order = new OrderBo();
            SalesItemBo item = new SalesItemBo(new ProductBo("Iphone"), 1, 10);
            SalesItemBo item2 = new SalesItemBo(new ProductBo("Galaxy"), 1, 10);
            order.AddItem(item);
            order.AddItem(item2);

            Assert.AreEqual(2, order.ItensQuantity);


        }

        [TestMethod]
        public void ItShouldAddWaysToPay()
        {
            OrderBo order = new OrderBo();
            order.InputWaysToPay(WaysToPay.InCash);
            Assert.AreEqual(WaysToPay.InCash, order.WaysToPay);

        }

        [TestMethod]
        public void OrderOneCoast10DollarsInCashToCost9Dollars()
        {

            OrderBo order = new OrderBo();
            order.InputWaysToPay(WaysToPay.InCash);
            order.AddItem(new SalesItemBo(new ProductBo("Notebook"), 1, 10));
            
            Assert.AreEqual(9, order.TotalValue);

        }

        [TestMethod]
        public void OrderTwoCoast18DollarsInCashToCost16_2Dollars()
        {

            OrderBo order = new OrderBo();
            order.InputWaysToPay(WaysToPay.InCash);
            order.AddItem(new SalesItemBo(new ProductBo("Notebook"), 2, 18));

            Assert.AreEqual(16.2m, order.TotalValue);

        }

        [TestMethod]
        public void OrderThreeCoast10DollarsCreditCardToCost10_3Dollars()
        {
            OrderBo order = new OrderBo();
            order.InputWaysToPay(WaysToPay.CreditCard);
            order.AddItem(new SalesItemBo(new ProductBo("Notebook"), 1, 10));

            Assert.AreEqual(10.3m, order.TotalValue);
        }

        [TestMethod]
        public void OrderFourCoast10DollarsCreditCardToCost10_3DollarsTwoProducts()
        {
            OrderBo order = new OrderBo();
            order.InputWaysToPay(WaysToPay.CreditCard);
            order.AddItem(new SalesItemBo(new ProductBo("Notebook"), 2, 20));

            Assert.AreEqual(20.6m, order.TotalValue);
        }

        /*Calcular valor total de um pedido*/
    }
}
