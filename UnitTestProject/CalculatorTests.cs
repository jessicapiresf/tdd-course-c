﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestDrivenDevelopment;

namespace UnitTestProject
{
    /// <summary>
    /// USC-001
    /// </summary>
    /// 
    [TestClass]
    public class CalculatorTests
    {
        
        public CalculatorTests()
        {            
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// Método de teste para soma de valores
        /// </summary>
        [TestMethod]
        public void SumTest()
        {
            Assert.AreEqual(Calculator.Sum(2, 2), 4);
        }

        /// <summary>
        /// Método de teste para soma de valores com uso de data pools test
        /// </summary>
        [DeploymentItem("TestDrivenDevelopment\\sum.csv"), DeploymentItem("TestDrivenDevelopment\\Sum.csv"), DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\Sum.csv", "Sum#csv", DataAccessMethod.Sequential), TestMethod()]
        public void SumTestWithDataPoolTest()
        {            
            Assert.AreEqual((int)TestContext.DataRow["Result"], Calculator.Sum((int)TestContext.DataRow["Value1"], (int)TestContext.DataRow["Value2"]));
        }


        /// <summary>
        /// Método de teste para soma de valores com metodos private
        /// </summary>
        [TestMethod]
        public void SumPrivateTest()
        {
            var privateObject = new PrivateObject(new Calculator());
            Assert.AreEqual((double)privateObject.Invoke("SumPrivate", 2, 2), 4);
        }


        /// <summary>
        /// Método de teste para soma de valores com metodos estaticos
        /// </summary>
        [TestMethod]
        public void SumStaticTest()
        {
            var privateType = new PrivateType(typeof(Calculator));
            Assert.AreEqual((double)privateType.InvokeStatic("SumStatic", 2, 2), 4);
        }


        /// <summary>
        /// Método de teste para subitração de valores
        /// </summary>
        [TestMethod]
        public void SubtracTestt()
        {
            Assert.AreEqual(Calculator.Subtract(2, 2), 0);
        }

        /// <summary>
        /// Método de teste para divisão de valores
        /// </summary>
        [TestMethod]
        public void DivisionTest()
        {
            Assert.AreEqual(Calculator.Division(2, 2), 1);
        }

        /// <summary>
        /// Método de teste para divisão de valores por zero
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(DivideByZeroException))]
        public void DivisionByZeroTest()
        {
            Calculator.Division(2, 0);
        }

        /// <summary>
        /// Método de teste para multiplicação de valores
        /// </summary>
        [TestMethod]
        public void MultiplyTest()
        {
            Assert.AreEqual(Calculator.Multiply(2, 2), 4);
        }
        
    }
}
