﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestDrivenDevelopment;

namespace UnitTestProject
{
    /// <summary>
    /// Summary description for PrimeNumbersTest
    /// </summary>
    [TestClass]
    public class PrimeNumbersTest
    {
        public PrimeNumbersTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        [TestInitialize]
        public void PreContiditions()
        {
            TestContext.WriteLine("Vou iniciar o método: " + TestContext.TestName);
        }

        [TestCleanup]
        public void PosContiditions()
        {
            TestContext.WriteLine("Executado o método: " + TestContext.TestName);
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
           } 
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// Metodo para estar numeros primos
        /// </summary>
        [TestMethod]
        public void GetPrimesNumbersTest()
        { 
            CollectionAssert.AreEqual(new [] { 1, 2, 3, 5, 7, 11 }, PrimeNumbers.GetPrimeNumbers());
        }
    }
}
