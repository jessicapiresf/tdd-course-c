﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioFinal
{
    public class OrderBo
    {
        #region Properties
        private IList<SalesItemBo> Itens { get; set; }
        public Guid Identifier { get; set; }
        public WaysToPay WaysToPay { get; private set; }
        public decimal TotalValue { get { return CalculateTotalValue(); } }
        public int ItensQuantity { get { return Itens.Count; } }
        #endregion

        #region Constructor
        public OrderBo()
        {
            Identifier = Guid.NewGuid();
            Itens = new List<SalesItemBo>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Método responsável por adicionar um iten de venda.
        /// </summary>
        /// <param name="item">Item de venda.</param>
        public void AddItem(SalesItemBo item)
        {
            Itens.Add(item);
        }

        /// <summary>
        /// Método responsável por colocar a forma de pagamento.
        /// </summary>
        /// <param name="waysToPay"></param>
        public void InputWaysToPay(WaysToPay waysToPay)
        {
            WaysToPay = waysToPay;
        }

        /// <summary>
        /// Método responsável por calcular o valor total a cobrar.
        /// </summary>
        /// <returns>Retorna o valor a cobrar.</returns>
        private decimal CalculateTotalValue()
        {
            decimal finalTotal = 0;

            if (WaysToPay.Equals(WaysToPay.InCash))
            {
                decimal total = Itens.Sum(x => x.TotalValueItem);
                finalTotal = total - (total * 0.1m);
            }

            if (WaysToPay.Equals(WaysToPay.CreditCard))
            {
                decimal total = Itens.Sum(x => x.TotalValueItem);
                finalTotal = total + (total * 0.03m);
            }

            return finalTotal;
        }
        #endregion
    }
}
