﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioFinal
{
    public class ProductBo
    {
        public string Name { get; set; }

        public ProductBo(string productName)
        {
            Name = productName;
        }
    }
}
