﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExercicioFinal
{

    public class SalesItemBo
    {
        public int Quantity { get; set; }
        public int TotalValueItem { get; set; }
        public ProductBo Product { get; set; }

        public SalesItemBo(ProductBo product, int quantity, int totalValue)
        {
            Product = product;
            Quantity = quantity;
            TotalValueItem = totalValue;
        }
    }
}
