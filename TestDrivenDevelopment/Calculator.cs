﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDrivenDevelopment
{
    /// <summary>
    /// Classe de Calculadora
    /// </summary>

    public class Calculator
    {
        /// <summary>
        /// Método para somar valores
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static double Sum(double x, double y)
        {
            return x + y;
        }
        

        /// <summary>
        /// Método para somar valores
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private double SumPrivate(double x, double y)
        {
            return x + y;
        }

        /// <summary>
        /// Método para somar valores
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        private static double SumStatic(double x, double y)
        {
            return x + y;
        }
        

        /// <summary>
        /// Método para subitrair valores
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static double Subtract(double x, double y)
        {
            return x - y;
        }

        /// <summary>
        /// Método para dividir valores
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static double Division(double x, double y)
        {
            if (y.Equals(0))
                throw new DivideByZeroException("Não existe divisão por zero.");

            return x / y;
        }

        /// <summary>
        ///  Multiplicar valores
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static double Multiply(double x, double y)
        {
            return x * y;
        }

    }
}
