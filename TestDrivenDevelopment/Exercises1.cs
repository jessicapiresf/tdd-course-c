﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDrivenDevelopment
{

    public class Cliente
    {
        public const char NivelSilver = 'S', NivelGold = 'G';
        public char NivelRelacionamento;

        public bool RelacionamentoSilver()
        {
            return NivelRelacionamento == NivelSilver;
        }

        public bool RelacionamentoGold()
        {
            return NivelRelacionamento == NivelGold;
        }
    }

    public abstract class Chamado
    {
        public Cliente Cliente { get; set; }
        public bool SituacaoCritica { get; set; }
    }

    public class Exercises1
    {
        public int GerarPrioridade(Chamado chamado)
        {   
            if (chamado.Cliente.NivelRelacionamento == Cliente.NivelSilver || chamado.Cliente.NivelRelacionamento == Cliente.NivelGold)       
                return chamado.SituacaoCritica ? 2 : 4;
                               
            return 5;            
        }
    }
}
