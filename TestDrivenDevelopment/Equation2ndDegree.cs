﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDrivenDevelopment
{
    /// <summary>
    /// Classe para gerar valores da formula de baskara
    /// </summary>
    public class Equation2ndDegree
    {

        /// <summary>
        /// Metodo de teste para gerar valores da formula de baskara
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static double[] getCalculateEsquation(double a, double b, double c)
        {
            if (a.Equals(0))
                throw new DivideByZeroException("Não existe divisão por zero.");            
            double[] result = new double[2];
            result[0] = ((-b) + (Math.Sqrt(Math.Pow(b, 2) - (4 * a * c)))) / (2 * a);
            result[1] = ((-b) - (Math.Sqrt(Math.Pow(b, 2) - (4 * a * c)))) / (2 * a);

            return result;
        }

    }
}
