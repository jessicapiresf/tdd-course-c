﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDrivenDevelopment
{
    public class PrimeNumbers
    {

        /// <summary>
        /// Metodo que retorna numeros primos
        /// </summary>
        /// <returns></returns>
        public static int[] GetPrimeNumbers()
        {
            return new[] {1 , 2, 3, 5, 7, 11 };
        }
    }
}
